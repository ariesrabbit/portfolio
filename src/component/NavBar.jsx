import Link from "next/link";
import React, { useEffect, useRef, useState } from "react";
import { Logo } from "./Logo";
import { useRouter } from "next/router";
import {
  FacebookIcon,
  GitLabIcon,
  LinkedInIcon,
  PinterestIcon,
  TwitterIcon,
} from "./Icon";
import { motion } from "framer-motion";

const CustomLink = ({ href, title, className = "" }) => {
  const router = useRouter();
  return (
    <Link href={href} className={`${className} relative group`}>
      {title}
      <span
        className={`h-[1px] inline-block w-0 bg-dark absolute left-0 -bottom-0.5 
        group-hover:w-full transition-[width] easa duration-300 ${
          router.asPath === href ? "w-full" : "w-0"
        }`}
      >
        &nbsp;
      </span>
    </Link>
  );
};
const CustomMobileLink = ({ href, title, className = "", toggle }) => {
  const router = useRouter();
  const handleClickMb = () => {
    toggle();
    router.push(href);
  };
  return (
    <button
      onClick={handleClickMb}
      href={href}
      className={`${className} relative group text-light my-2`}
    >
      {title}
      <span
        className={`h-[1px] inline-block w-0 bg-light absolute left-0 -bottom-0.5 
        group-hover:w-full transition-[width] easa duration-300 ${
          router.asPath === href ? "w-full" : "w-0"
        }`}
      >
        &nbsp;
      </span>
    </button>
  );
};

export const NavBar = () => {
  const ref = useRef(null);

  useEffect(() => {
    document.addEventListener("click", handleClickOutside);
    return () => {
      document.removeEventListener("click", handleClickOutside);
    };
  }, []);

  const handleClickOutside = (event) => {
    if (ref.current && !ref.current.contains(event.target)) {
      setIsOpen(false);
    }
  };
  const [isOpen, setIsOpen] = useState(false);
  const handleClick = (event) => {
    if (event) {
      event.stopPropagation();
    }
    setIsOpen(!isOpen);
  };
  return (
    <header
      className="w-full px-32 py-8 font-medium flex items-center justify-between relative
    xl:px-24 lg:px-16 md:px-12 sm:px-8"
    >
      <button
        onClick={handleClick}
        className="flex-col justify-center items-center hidden lg:flex"
      >
        <span
          className={`bg-dark block transition-all duration-300 ease-out h-0.5 w-6 rounded-sm ${
            isOpen ? "rotate-45 translate-y-1" : "-translate-y-0.5"
          }`}
        ></span>
        <span
          className={`bg-dark block transition-all duration-300 ease-out h-0.5 w-6 rounded-sm my-0.5 ${
            isOpen ? "opacity-0" : "opacity-100"
          }`}
        ></span>
        <span
          className={`bg-dark block transition-all duration-300 ease-out h-0.5 w-6 rounded-sm ${
            isOpen ? "-rotate-45 -translate-y-1" : "translate-y-0.5"
          }`}
        ></span>
      </button>
      <div className="w-full flex items-center justify-between lg:hidden">
        <nav>
          <CustomLink href="/" title="Home" className="mr-6" />
          <CustomLink href="/about" title="About" className="mr-6" />
          <CustomLink href="/projects" title="Projects" className="mr-6" />
          {/* <CustomLink href="/articles" title="Articles" className="mr-4" /> */}
        </nav>
        <nav className="flex-shrink-0 flex items-center justify-center flex-wrap">
          <motion.a
            href="https://www.facebook.com/t.rabbitblack"
            target={"_blank"}
            whileHover={{ scale: 1.5 }}
            whileTap={{ scale: 0.9 }}
            className="w-6 mr-2"
          >
            <FacebookIcon />
          </motion.a>
          <motion.a
            href="https://gitlab.com/ariesrabbit"
            target={"_blank"}
            whileHover={{ scale: 1.5 }}
            whileTap={{ scale: 0.9 }}
            className="w-6 mx-2"
          >
            <GitLabIcon />
          </motion.a>
          <motion.a
            href="https://www.linkedin.com/in/congthanhdao2703"
            target={"_blank"}
            whileHover={{ scale: 1.5 }}
            whileTap={{ scale: 0.9 }}
            className="w-6 mx-2"
          >
            <LinkedInIcon />
          </motion.a>
          <motion.a
            href="/"
            target={"_blank"}
            whileHover={{ scale: 1.5 }}
            whileTap={{ scale: 0.9 }}
            className="w-6 mx-2"
          >
            <TwitterIcon />
          </motion.a>
          <motion.a
            href="/"
            target={"_blank"}
            whileHover={{ scale: 1.5 }}
            whileTap={{ scale: 0.9 }}
            className="w-6 ml-2"
          >
            <PinterestIcon />
          </motion.a>
        </nav>
      </div>
      {isOpen ? (
        <motion.div
          ref={ref}
          initial={{ scale: 0, opacity: 0, x: "-50%", y: "-50%" }}
          animate={{ scale: 1, opacity: 1 }}
          transition="0.2s"
          className="min-w-[90vw] z-50 flex flex-col items-center justify-between fixed top-1/2 left-1/2 
      -translate-x-1/2 -translate-y-1/2 bg-dark/90 rounded-lg backdrop-blur-md py-32"
        >
          <nav className="flex flex-col items-center justify-center">
            <CustomMobileLink
              href="/"
              title="Home"
              className=""
              toggle={handleClick}
            />
            <CustomMobileLink
              href="/about"
              title="About"
              className=""
              toggle={handleClick}
            />
            <CustomMobileLink
              href="/projects"
              title="Projects"
              className=""
              toggle={handleClick}
            />
            {/* <CustomLink href="/articles" title="Articles" className="mr-4" /> */}
          </nav>
          <nav className="flex-shrink-0 flex items-center justify-center flex-wrap mt-2">
            <motion.a
              href="https://www.facebook.com/t.rabbitblack"
              target={"_blank"}
              whileHover={{ scale: 1.5 }}
              whileTap={{ scale: 0.9 }}
              className="w-6 mr-2 bg-light rounded-full sm:mx-1"
            >
              <FacebookIcon />
            </motion.a>
            <motion.a
              href="https://gitlab.com/ariesrabbit"
              target={"_blank"}
              whileHover={{ scale: 1.5 }}
              whileTap={{ scale: 0.9 }}
              className="w-6 mx-2 sm:mx-1"
            >
              <GitLabIcon />
            </motion.a>
            <motion.a
              href="https://www.linkedin.com/in/congthanhdao2703"
              target={"_blank"}
              whileHover={{ scale: 1.5 }}
              whileTap={{ scale: 0.9 }}
              className="w-6 mx-2 sm:mx-1"
            >
              <LinkedInIcon />
            </motion.a>
            <motion.a
              href="/"
              target={"_blank"}
              whileHover={{ scale: 1.5 }}
              whileTap={{ scale: 0.9 }}
              className="w-6 mx-2 sm:mx-1"
            >
              <TwitterIcon />
            </motion.a>
            <motion.a
              href="/"
              target={"_blank"}
              whileHover={{ scale: 1.5 }}
              whileTap={{ scale: 0.9 }}
              className="w-6 ml-2 sm:mx-1"
            >
              <PinterestIcon />
            </motion.a>
          </nav>
        </motion.div>
      ) : null}
      <div className="absolute left-[50%] top-2 translate-x-[-50%]">
        {/* <Logo /> */}
      </div>
    </header>
  );
};
