import React from "react";
import { CircularTextFE } from "./Icon";
import Link from "next/link";

export const HireMe = () => {
  return (
    <div
      className="fixed left-3 bottom-3 flex items-center justify-center overflow-hidden md:right-0 md:left-auto 
      md:top-0 md:bottom-auto md:absolute
    sm:right-0"
    >
      <div className="w-44 h-auto flex items-center justify-center relative md:w-24">
        <CircularTextFE className={"fill-dark animate-spin-slow"} />
        <Link
          href="mailto:anh.thanh1400@gmail.com"
          className="flex items-center justify-center absolute left-1/2 top-1/2 -translate-x-1/2 -translate-y-1/2
             bg-black text-light shadow-md border border-solid border-dark h-20 w-20 rounded-full z-10 font-semibold
             transition duration-100
             hover:bg-light hover:text-dark
             md:w-12 md:h-12 md:text-[10px]"
        >
          Hire Me
        </Link>
      </div>
    </div>
  );
};
