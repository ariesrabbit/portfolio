import React, { useRef } from "react";
import { motion, useScroll } from "framer-motion";
import { LilIcon } from "./LilIcon";

const Details = ({ place, company, time, work }) => {
  const ref = useRef(null);

  return (
    <li
      ref={ref}
      className="my-8 first:mt-0 last:mb-0 w-3/5 mx-auto flex items-center justify-center md:w-4/5 
      md:text-6xl xs:text-4xl md:mb-16"
    >
      <LilIcon reference={ref} />
      <motion.div
        initial={{ y: 50 }}
        whileInView={{ y: 0 }}
        transition={{ duration: 0.5, type: "spring" }}
      >
        <h3 className="capitalize font-bold text-2xl sm:text-xl xs:text-lg">
          {place}&nbsp;
          <span className="text-primary capitalize">{company}</span>
        </h3>
        <span className="capitalize font-medium text-dark/75 xs:text-sm">
          | {time} |
        </span>
        <p className="font-medium w-full md:text-sm">{work}</p>
      </motion.div>
    </li>
  );
};

export const Education = () => {
  const ref = useRef(null);
  const { scrollYProgress } = useScroll({
    target: ref,
    offset: ["start end", "center start"],
  });
  return (
    <div className="my-64">
      <h2 className="font-bold text-8xl mb-32 w-full text-center md:text-6xl xs:text-4xl md:mb-16">
        Education
      </h2>
      <div ref={ref} className="w-3/4 mx-auto relative lg:w-[90%] md:w-full">
        <motion.div
          style={{ scaleY: scrollYProgress }}
          className="absolute left-9 top-0 w-[4px] h-full bg-dark origin-top md:w-[2px] md:left-[30px] xs:left-[20px]"
        />
        <ul className="w-full flex flex-col items-start justify-center ml-4 xl:ml-2">
          <Details
            place="Industrial University of Ho Chi Minh city"
            company=""
            time="2018 - 2022"
            work={
              <>
                Graduated in Information Technology.
                <br />
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam.
              </>
            }
          />
          <Details
            place="Cyber Soft"
            company=""
            time="June 2022 - Dec 2022"
            work={
              <>
                Bootcamp FrontEnd Developer.
                <br />
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam.
              </>
            }
          />
        </ul>
      </div>
    </div>
  );
};
