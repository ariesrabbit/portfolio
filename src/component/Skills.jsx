import React from "react";
import { motion } from "framer-motion";

const Skill = ({ name, x, y }) => {
  return (
    <motion.div
      className="flex items-center justify-center rounded-full font-semibold 
        bg-dark text-light px-3 py-3 shadow-dark cursor-pointer absolute
        lg:py-2 lg:px-4 md:text-sm md:py-1.5 md:px-3 xs:bg-transparent xs:text-dark xs:font-bold"
      whileHover={{
        scale: 1.05,
      }}
      initial={{ x: 0, y: 0 }}
      whileInView={{ x: x, y: y }}
      transition={{ duration: 1.5 }}
      viewport={{ once: true }}
    >
      {name}
    </motion.div>
  );
};

export const Skills = () => {
  return (
    <>
      <h2 className="font-bold text-8xl mt-64 w-full text-center md:text-6xl md:mt-32">
        Skills
      </h2>
      <div
        className="w-full h-screen relative flex items-center justify-center rounded-full bg-circularLight
      lg:h-[80vh] sm:h-[60vh] xs:h-[50vh]
      lg:bg-circularLightLg md:bg-circularLightMd sm:bg-circularLightSm"
      >
        <motion.div
          className="flex items-center justify-center rounded-full font-semibold 
        bg-dark text-light p-8 shadow-dark cursor-pointer lg:p-6  md:p-4
        xs:text-sm xs:p-2"
          whileHover={{
            scale: 1.05,
            backgroundColor: [
              "#121212",
              "rgba(131,58,180,1)",
              "rgba(253,29,29,1)",
              "rgba(252,176,69,1)",
              "rgba(131,58,180,1)",
              "#121212",
            ],
            transition: { duration: 1, repeat: Infinity },
          }}
        >
          Web
        </motion.div>
        <Skill name="HTML" x="-19vw" y="2vw" />
        <Skill name="CSS/SCSS" x="-5vw" y="-10vw" />
        <Skill name="JavaScript" x="17vw" y="6vw" />
        <Skill name="ReactJS" x="0vw" y="14vw" />
        <Skill name="AJAX" x="-20vw" y="-15vw" />
        <Skill name="Responsive" x="15vw" y="-12vw" />
        <Skill name="Web Design" x="32vw" y="-5vw" />
        <Skill name="Figma" x="0vw" y="-18vw" />
        <Skill name="Tailwind CSS" x="-32vw" y="12vw" />
      </div>
    </>
  );
};
