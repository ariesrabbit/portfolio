import { Layout } from "@/component/Layout";
import Head from "next/head";
import Image from "next/image";
import profilePic from "../../public/images/profile/devanime2_5.png";
import { useRef, useState } from "react";
import { AnimatedText } from "@/component/AnimatedText";
import { HireMe } from "@/component/HireMe";
import lightBulb from "../../public/images/svgs/miscellaneous_icons_1.svg";
import TransitionEffect from "@/component/TransitionEffect";

export default function Home() {
  const tiltRef = useRef(null);

  const handleMove = (e) => {
    const el = tiltRef.current;

    const height = el.clientHeight;
    const width = el.clientWidth;

    const xVal = e.nativeEvent.layerX;
    const yVal = e.nativeEvent.layerY;

    const yRotation = 20 * ((xVal - width / 2) / width);
    const xRotation = -20 * ((yVal - height / 2) / height);

    const string =
      "perspective(500px) scale(1.1) rotateX(" +
      xRotation +
      "deg) rotateY(" +
      yRotation +
      "deg)";

    el.style.transform = string;
  };

  const handleMouseOut = () => {
    const el = tiltRef.current;
    el.style.transform = "perspective(500px) scale(1) rotateX(0) rotateY(0)";
  };

  const handleMouseDown = () => {
    const el = tiltRef.current;
    el.style.transform = "perspective(500px) scale(0.9) rotateX(0) rotateY(0)";
  };

  const handleMouseUp = () => {
    const el = tiltRef.current;
    el.style.transform = "perspective(500px) scale(1.1) rotateX(0) rotateY(0)";
  };
  return (
    <>
      <Head>
        <title>Đào Công Thành</title>
        <meta name="description" content="My portfolio" />
      </Head>
      <TransitionEffect />
      <main className="flex items-center text-dark w-full min-h-screen">
        <Layout className="pt-0 md:pt-16 sm:pt-8">
          <div className="flex items-center justify-between w-full lg:flex-col">
            <div className="w-1/2 flex justify-center items-center md:w-full">
              <div
                id="rotateImg"
                ref={tiltRef}
                onMouseMove={handleMove}
                onMouseOut={handleMouseOut}
                onMouseDown={handleMouseDown}
                onMouseUp={handleMouseUp}
                className="p-1 h-auto mx-auto lg:hidden md:inline-block md:w-2/3 md:mb-4"
              >
                <Image
                  src={profilePic}
                  alt="Pic of me gen by AI"
                  className="w-full h-auto rounded-2xl"
                  priority
                  sizes="(max-width: 768px) 100vw,
              (max-width: 1200px) 50vw,
              33vw"
                />
              </div>
            </div>
            <div className="w-1/2 flex flex-col items-center self-center lg:w-full lg:text-center">
              <AnimatedText
                text="Turning Vision Into Reality With Code And Design."
                className="text-6xl text-left xl:text-5xl lg:text-center lg:text-6xl md:text-5xl sm:text-3xl"
              />
              <p className="mb-4 text-base font-medium">
                As a front-end developer, I am dedicated to turning ideas into
                innovative web applications. Explore my latest projects,
                showcasing my expertise in React.js and web development.
              </p>
            </div>
          </div>
        </Layout>
        <HireMe />
        <div className="absolute right-8 bottom-4 inline-block w-24 md:hidden">
          <Image src={lightBulb} alt="idea" className="w-full h-auto " />
        </div>
      </main>
    </>
  );
}
