import { AnimatedText } from "@/component/AnimatedText";
import { Layout } from "@/component/Layout";
import Head from "next/head";
import Image from "next/image";
import React, { useEffect, useRef } from "react";
import profilePic from "../../public/images/profile/me.jpg";
import { useInView, useMotionValue, useSpring } from "framer-motion";
import { Skills } from "@/component/Skills";
import { Experience } from "@/component/Experience";
import { Education } from "@/component/Education";
import TransitionEffect from "@/component/TransitionEffect";

const AnimatedNumber = ({ value }) => {
  const ref = useRef(null);
  const motionValue = useMotionValue(0);
  const springValue = useSpring(motionValue, { duration: 3000 });
  const isInview = useInView(ref, { once: true });
  useEffect(() => {
    if (isInview) {
      motionValue.set(value);
    }
  }, [isInview, value]);
  useEffect(() => {
    springValue.on("change", (latest) => {
      if (ref.current && latest.toFixed(0) <= value) {
        ref.current.textContent = latest.toFixed(0);
      }
    });
  }, []);
  return <span ref={ref}></span>;
};

const About = () => {
  return (
    <>
      <Head>
        <title>Me | About Page</title>
        <meta name="description" content="any description" />
      </Head>
      <TransitionEffect />
      <main className="flex w-full flex-col items-center justify-center">
        <Layout className="pt-16">
          <AnimatedText
            text="Passion Fuels Purpose!"
            className="text-8xl mb-16 lg:text-7xl sm:text-6xl xs:text-4xl sm:mb-8"
          />
          <div className="grid w-full grid-cols-8 gap-16 sm:gap-8">
            <div className="col-span-3 flex flex-col items-start justify-start xl:col-span-4 md:order-2 md:col-span-8">
              <h2 className="mb-4 text-lg font-bold uppercase text-dark/75">
                Biography
              </h2>
              <p className="font-medium">
                Being a frontend developer, my primary focus is on creating
                visually appealing and user-friendly websites that engage and
                delight visitors. With a strong background in web development
                technologies such as HTML, CSS, and JavaScript, I am
                well-equipped to develop responsive and dynamic web applications
                that enhance user experience.
              </p>
              <p className="font-medium my-4">
                I possess a solid understanding of web design principles and
                stay up-to-date with the latest industry trends and
                technologies. I am skilled in using popular frontend frameworks
                like React, Angular, and Vue.js to build scalable and modular
                applications.
              </p>
              <p className="font-medium">
                Overall, I am a self-motivated, detail-oriented frontend
                developer with a passion for creating elegant and functional
                websites that make a difference in the lives of users.
              </p>
            </div>
            <div
              className="col-span-3 relative h-max rounded-2xl border-2 border-solid border-dark bg-light p-8 
            xl:col-span-4 md:order-1 md:col-span-8"
            >
              <div className="absolute top-0 -right-3 -z-10 w-[102%] h-[103%] rounded-[2em] bg-dark "></div>
              <div className="relative group overflow-hidden">
                <div
                  className="absolute h-full w-1/2 bg-gradient-to-r from-transparent to-white opacity-30 
                -left-80 top-0 transform -skew-x-[35deg] transition-all duration-500 group-hover:left-96"
                ></div>
                <Image
                  src={profilePic}
                  alt="Dao Cong Thanh"
                  className="w-full h-auto rounded-2xl"
                  priority
                  sizes="(max-width: 768px) 100vw,
              (max-width: 1200px) 50vw,
              33vw"
                />
              </div>
            </div>
            <div className="col-span-2 flex flex-col items-end justify-between xl:col-span-8 xl:flex-row xl:items-center md:order-3">
              <div className="flex flex-col items-end justify-center xl:items-center">
                <span className="inline-block text-7xl font-bold md:text-6xl sm:text-5xl xs:text-4xl">
                  <AnimatedNumber value={10} />+
                </span>
                <h2 className="text-xl font-medium capitalize text-dark/75 xl:text-center md:text-lg sm:text-base xs:text-sm">
                  satisfied clients
                </h2>
              </div>
              <div className="flex flex-col items-end justify-center xl:items-center">
                <span className="inline-block text-7xl font-bold md:text-6xl sm:text-5xl xs:text-4xl">
                  <AnimatedNumber value={10} />+
                </span>
                <h2 className="text-xl font-medium capitalize text-dark/75 xl:text-center md:text-lg sm:text-base xs:text-sm">
                  projects completed
                </h2>
              </div>
              <div className="flex flex-col items-end justify-center xl:items-center">
                <span className="inline-block text-7xl font-bold md:text-6xl sm:text-5xl xs:text-4xl">
                  <AnimatedNumber value={1} />+
                </span>
                <h2 className="text-xl font-medium capitalize text-dark/75 xl:text-center md:text-lg sm:text-base xs:text-sm">
                  years of experience
                </h2>
              </div>
            </div>
          </div>
          <Skills />
          <Experience />
          <Education />
        </Layout>
      </main>
    </>
  );
};
export default About;
