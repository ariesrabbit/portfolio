import { AnimatedText } from "@/component/AnimatedText";
import { Layout } from "@/component/Layout";
import Head from "next/head";
import Image from "next/image";
import Link from "next/link";
import React from "react";
import imgProject1 from "../../public/images/projects/Bizchat-thumbnail.png";
import imgProject2 from "../../public/images/projects/Doc-thumbnail.png";
import imgProject3 from "../../public/images/projects/crop1.png";
import imgProject4 from "../../public/images/projects/crop2.png";
import { LinkArrow } from "@/component/Icon";
import TransitionEffect from "@/component/TransitionEffect";

const FeaturedProject = ({
  type,
  title,
  summary,
  img,
  link,
  available = true,
}) => {
  return (
    <article
      className="w-full flex items-center justify-between rounded-br-2xl
    rounded-3xl border border-solid border-dark bg-light shadow-2xl p-12 relative
    lg:flex-col lg:p-8 xs:rounded-br-3xl xs:p-4"
    >
      <div
        className="absolute top-0 -right-3 -z-10 w-[101%] h-[103%] rounded-[2.5em] bg-dark rounded-br-3xl
      xs:right-2 xs:h-[102%] xs:w-full xs:rounded-[1.5rem]"
      ></div>

      <div class="container_card w-1/2 lg:w-full">
        <div class="card">
          <div class="front">
            <Link
              href={link}
              target="_blank"
              className=" cursor-pointer overflow-hidden rounded-lg"
            >
              <Image
                src={img}
                alt={title}
                className="w-full h-full rounded-2xl"
                priority
                sizes="(max-width: 768px) 100vw,
            (max-width: 1200px) 50vw,
            33vw"
              />
            </Link>
          </div>
          <div class="back text-dark">
            <h1 className="underline underline-offset-2 mb-2">Techs stack:</h1>
            <p className="w-1/2 italic">
              ReactJS, Hook, ReduxToolkit, React Router, SCSS, Module CSS,
              Tailwind CSS, AJAX, Firebase, Responsive
            </p>
          </div>
        </div>
      </div>

      <div className="w-1/2 flex flex-col items-start justify-between pl-6 lg:w-full lg:pl-0 lg:pt-6">
        <span className="text-primary font-medium text-xl">{type}</span>
        <h2 className="my-2 w-full text-left text-4xl font-bold underline underline-offset-2 sm:text-sm">
          {title}
        </h2>
        <p className="my-2 font-medium text-dark sm:text-sm">{summary}</p>
        {available && (
          <div>
            <span className="navbar_text">
              <button>
                <Link
                  href={link}
                  target="_blank"
                  className="flex items-center gap-1 sm:text-base"
                >
                  Visit Project
                  <div className="w-4">
                    <LinkArrow />
                  </div>
                </Link>
              </button>
            </span>
          </div>
        )}
      </div>
    </article>
  );
};

const Projects = () => {
  return (
    <>
      <Head>
        <title>Me | Project Page</title>
        <meta name="description" content="any description" />
      </Head>
      <TransitionEffect />
      <main className="w-full mb-16 flex flex-col items-center justify-center">
        <Layout className="pt-16">
          <AnimatedText
            text="Imagination Trumps Knowledge!"
            className="text-8xl mb-16 lg:text-7xl sm:text-6xl xs:text-4xl"
          />
          <div className="grid grid-cols-12 gap-24 gap-y-32 xl:gap-x-16 lg:gap-x-8 md:gap-y-24 sm:gap-x-0">
            <div className="col-span-12">
              <FeaturedProject
                title="BizChat"
                img={imgProject1}
                summary="Welcome to BizChat, dedicated to the exciting world of chatbot AI! Here, you'll find everything you need to know about chatbots, from the basics of how they work to advanced techniques."
                available={false}
                link="/"
                type="Featured Project"
              />
            </div>
            <div className="col-span-12">
              <FeaturedProject
                title="DocTranslator "
                img={imgProject2}
                summary="Welcome to DocTranslator, where we use the latest AI technology to provide accurate and reliable document translation services. Whether you need to translate legal documents, business reports, or personal correspondence, our AI-powered platform can handle it all."
                link="https://doctranslate.io/"
                type="Featured Project"
              />
            </div>
            <div className="col-span-12">
              <FeaturedProject
                title="Samnam"
                img={imgProject3}
                summary={`${"Introducing Special Ngoc Linh Ginseng - a precious product with unparalleled nutritional and health benefits. Ngoc Linh Ginseng, known as the 'jewel of the mountain,' is a treasure found in the highland region of Dak Nong, Vietnam."}`}
                link="https://samnam.appbeta.info/"
                type="Freelance Project"
              />
            </div>
            <div className="col-span-12">
              <FeaturedProject
                title="Enfarm "
                img={imgProject4}
                summary="enfarm Agritech was founded by a group of IT and environmental engineers who believe that smart fertilising and connecting farmers to the market will improve the livelihoods of millions of Vietnamese farmers and help the agriculture sector grow sustainably."
                link="https://enfarm.com/"
                type="Freelance Project"
              />
            </div>
          </div>
        </Layout>
      </main>
    </>
  );
};

export default Projects;
